
docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file pom.xml

docker pull maven:3.8.2-openjdk-11

docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3-jdk-11 mvn clean package --file apix-microservices-account/apix-microservices-customer/pom.xml


  aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 613739954613.dkr.ecr.us-east-2.amazonaws.com


docker build -t apix2021/microservices-account .
docker tag apix2021/microservices-account:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:latest
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:latest

docker build -t apix2021/microservices-notification .
docker tag apix2021/microservices-notification:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:latest
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:latest



docker build -t apix2021/microservices-customer .
docker tag apix2021/microservices-customer:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:latest
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:latest



 helm install apix-account apix-account/ --values apix-account/values-dev.yaml  -n microservices-sandbox

 helm install apix-customer apix-customer/ --values apix-customer/values-dev.yaml  -n microservices-sandbox

 helm install apix-notification apix-notification/ --values apix-notification/values-dev.yaml  -n microservices-sandbox



 helm upgrade apix-account apix-account/ --values apix-account/values-dev.yaml  -n microservices-sandbox

 helm upgrade apix-customer apix-customer/ --values apix-customer/values-dev.yaml  -n microservices-sandbox

 helm upgrade apix-notification apix-notification/ --values apix-notification/values-dev.yaml  -n microservices-sandbox

 kubectl apply -f sandbox-ingress.yaml -n microservices-sandbox


 kubectl create secret generic rabb ti-staff --from-literal=host='' --from-literal=username='' --from-literal=password='' -n microservices-sandbox


 kubectl create secret generic documentdb-staff --from-literal=database_mongodb_uri=''  -n microservices-sandbox




 curl --location --request POST 'http://localhost:8080/customers' \
 --header 'Content-Type: application/json' \
 --data-raw '{
     "first_name": "Cassiane",
     "last_name": "Santos",
     "document": "22255595000",
     "gross_salary": 3000.00
 }'





 sh

export BASE_NAME="pimenta-pr"

docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-account/pom.xml

docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-customer/pom.xml

docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8.2-openjdk-11 mvn clean package --file apix-microservices-notification/pom.xml



aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 613739954613.dkr.ecr.us-east-2.amazonaws.com


docker build -t apix2021/microservices-account .
docker tag apix2021/microservices-account:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-account:$BASE_NAME

docker build -t apix2021/microservices-notification .
docker tag apix2021/microservices-notification:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-notification:$BASE_NAME



docker build -t apix2021/microservices-customer .
docker tag apix2021/microservices-customer:latest 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:$BASE_NAME
docker push 613739954613.dkr.ecr.us-east-2.amazonaws.com/apix2021/microservices-customer:$BASE_NAME

sed k8s/bundle/values-dev.yaml -e "s/latest/$BASE_NAME/g" > k8s/bundle/values-pipeline.yaml

helm install apix-$BASE_NAME k8s/bundle/ --values k8s/bundle//values-dev.yaml  -n microservices-apix
