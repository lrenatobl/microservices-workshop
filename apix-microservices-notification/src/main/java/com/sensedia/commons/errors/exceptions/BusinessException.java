package com.sensedia.commons.errors.exceptions;

import org.springframework.http.HttpStatus;

public class BusinessException extends ApplicationException {

  public BusinessException() {
    this(null, null, null, null);
  }

  public BusinessException(String detail) {
    this(detail, null, null, null);
  }

  public BusinessException(String detail, String type) {
    this(detail, type, null, null);
  }

  public BusinessException(String detail, String type, String title) {
    this(detail, type, title, null);
  }

  public BusinessException(Throwable cause) {
    this(null, cause);
  }

  public BusinessException(String detail, Throwable cause) {
    this(detail, null, cause);
  }

  public BusinessException(String detail, String type, Throwable cause) {
    this(detail, type, null, cause);
  }

  public BusinessException(String detail, String type, String title, Throwable cause) {
    super(HttpStatus.UNPROCESSABLE_ENTITY, detail, type, title, cause);
  }
}
